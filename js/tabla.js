/*Declaracion de los Arreglos Titulo y Datos*/

Titulo=["#","Numero de Control","Nombre"];

var Datos = new Array(new Array ("01","10160838","Hernandez Romero Guillermo Erik"), 
                      new Array("02","10160839","Perez Galvan Marco"),
                      new Array("03","10160840","Martinez Coto Juan")); 


/*Funcion llenar_tabla*/
function llenar_tabla(titulo,datos){
var table = document.createElement('table');

var th = document.createElement("thead");
table.appendChild(th);

var tblBody = document.createElement("tbody");

    for (var i=0; i<titulo.length;i++){
         var textNode = document.createTextNode(titulo[i])
         var td=document.createElement('td');
         td.appendChild(textNode);
         th.setAttribute("id","titulos");
         th.appendChild(td);
    }

 for (var i = 0; i < datos.length; i++) {

    var hilera = document.createElement("tr");
 
    for (var j = 0; j < datos.length; j++) {

      var celda = document.createElement("td");
      var textoCelda = document.createTextNode(datos[i][j]);
      celda.appendChild(textoCelda);
      hilera.appendChild(celda);
    }

    tblBody.appendChild(hilera);
  }

table.appendChild(tblBody);
document.body.appendChild(table);
table.setAttribute("border","1");
}

/*Probando funcion*/
llenar_tabla(Titulo,Datos);



